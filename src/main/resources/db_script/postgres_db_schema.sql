CREATE TABLE auth_token
(
  token character varying(40) NOT NULL,
  CONSTRAINT auth_token_pk PRIMARY KEY (token)
);

CREATE TABLE nasdaq_data
(
  nasdaq_date date NOT NULL,
  nasdaq_raw_data text,
  created timestamp with time zone DEFAULT now(),
  updated timestamp with time zone,
  CONSTRAINT nasdaq_data_pk PRIMARY KEY (nasdaq_date)
);