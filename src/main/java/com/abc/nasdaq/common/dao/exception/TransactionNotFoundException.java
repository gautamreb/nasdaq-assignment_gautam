package com.abc.nasdaq.common.dao.exception;

public class TransactionNotFoundException extends Exception
{
    private static final long serialVersionUID = -4289995836177231857L;
}
