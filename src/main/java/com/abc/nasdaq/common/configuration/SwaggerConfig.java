package com.abc.nasdaq.common.configuration;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.mangofactory.swagger.configuration.SpringSwaggerConfig;
import com.mangofactory.swagger.models.dto.ApiInfo;
import com.mangofactory.swagger.plugin.EnableSwagger;
import com.mangofactory.swagger.plugin.SwaggerSpringMvcPlugin;

@Configuration
@EnableSwagger
public class SwaggerConfig
{
    @Autowired
    private SpringSwaggerConfig springSwaggerConfig;

    @Autowired
    private ServletContext servletContext;

    @Bean
    public SwaggerSpringMvcPlugin customImplementation()
    {
	return new SwaggerSpringMvcPlugin(this.springSwaggerConfig).apiInfo(apiInfo())
		.includePatterns(".*/.*").pathProvider(new RelativeSwaggerPathProvider(
			servletContext));
    }

    private ApiInfo apiInfo()
    {
	ApiInfo apiInfo = new ApiInfo("ABC Tech Assignment", "NASDAQ API", null, null, null, null);
	return apiInfo;
    }

}
