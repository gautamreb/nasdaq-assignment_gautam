package com.abc.nasdaq.common.exception;

public class AuthenticationFailException extends Exception
{
    private static final long serialVersionUID = 8576210682193016645L;
}
