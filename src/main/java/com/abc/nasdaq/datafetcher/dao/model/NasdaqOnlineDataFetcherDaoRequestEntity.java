package com.abc.nasdaq.datafetcher.dao.model;

public class NasdaqOnlineDataFetcherDaoRequestEntity
{
    private String url;
    private String index;
    private int connectTimeout;
    private int readTimeout;

    public String getUrl()
    {
	return url;
    }

    public void setUrl(String url)
    {
	this.url = url;
    }

    public String getIndex()
    {
	return index;
    }

    public void setIndex(String index)
    {
	this.index = index;
    }

    public int getConnectTimeout()
    {
	return connectTimeout;
    }

    public void setConnectTimeout(int connectTimeout)
    {
	this.connectTimeout = connectTimeout;
    }

    public int getReadTimeout()
    {
	return readTimeout;
    }

    public void setReadTimeout(int readTimeout)
    {
	this.readTimeout = readTimeout;
    }
}
