package com.abc.nasdaq.datafetcher.dao.model;

import com.abc.nasdaq.model.NasdaqDataResponse;

public class NasdaqOnlineDataFetcherDaoResponseEntity
{
    private NasdaqDataResponse nasdaqDataResponse;

    public NasdaqDataResponse getNasdaqDataResponse()
    {
	return nasdaqDataResponse;
    }

    public void setNasdaqDataResponse(NasdaqDataResponse nasdaqDataResponse)
    {
	this.nasdaqDataResponse = nasdaqDataResponse;
    }

    @Override
    public String toString()
    {
	return "NasdaqOnlineDataFetcherDaoResponseEntity [nasdaqDataResponse=" + nasdaqDataResponse + "]";
    }

}
