package com.abc.nasdaq.datafetcher.dao.model;

import java.util.Date;

public class NasdaqDataUpdateDaoRequestEntity
{
    private Date targetDate;
    private String rawData;

    public Date getTargetDate()
    {
	return targetDate;
    }

    public void setTargetDate(Date targetDate)
    {
	this.targetDate = targetDate;
    }

    public String getRawData()
    {
	return rawData;
    }

    public void setRawData(String rawData)
    {
	this.rawData = rawData;
    }

    @Override
    public String toString()
    {
	return "NasdaqDataInsertDaoRequestEntity [targetDate=" + targetDate + ", rawData=" + rawData + "]";
    }

}
