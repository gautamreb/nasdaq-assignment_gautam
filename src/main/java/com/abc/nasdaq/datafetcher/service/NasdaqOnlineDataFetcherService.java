package com.abc.nasdaq.datafetcher.service;

import java.util.Date;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.abc.nasdaq.datafetcher.dao.NasdaqDataUpdateDaoJpaImpl;
import com.abc.nasdaq.datafetcher.dao.NasdaqOnlineDataFetcherDaoRestImpl;
import com.abc.nasdaq.datafetcher.dao.model.NasdaqDataInsertDaoRequestEntity;
import com.abc.nasdaq.datafetcher.dao.model.NasdaqDataUpdateDaoRequestEntity;
import com.abc.nasdaq.datafetcher.dao.model.NasdaqOnlineDataFetcherDaoRequestEntity;
import com.abc.nasdaq.datafetcher.dao.model.NasdaqOnlineDataFetcherDaoResponseEntity;
import com.abc.nasdaq.enquiry.dao.NasdaqDataInsertDaoJpaImpl;
import com.abc.nasdaq.model.NasdaqDataResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class NasdaqOnlineDataFetcherService
{
    @Value("${nasdaq.data.url}")
    private String nasdaqDataUrl;

    @Value("${nasdaq.data.encoded.request.form.index}")
    private String nasdaqEncodedFormDataIndex;

    @Value("${nasdaq.data.connect.timeout}")
    private int connectTimeout;

    @Value("${nasdaq.data.read.timeout}")
    private int readTimeout;

    @Autowired
    private NasdaqOnlineDataFetcherDaoRestImpl nasdaqOnlineDataFetcherDaoRestImpl;

    @Autowired
    private NasdaqDataInsertDaoJpaImpl nasdaqDataInsertDaoJpaImpl;

    @Autowired
    private NasdaqDataUpdateDaoJpaImpl nasdaqDataUpdateDaoJpaImpl;

    public void doService()
    {
	NasdaqOnlineDataFetcherDaoResponseEntity nasdaqOnlineDataFetcherDaoResponse = null;
	String nasdaqRawDataJsonString = null;
	try
	{
	    nasdaqOnlineDataFetcherDaoResponse = fetchNasdaqData();
	    nasdaqRawDataJsonString = convertDataToJsonString(nasdaqOnlineDataFetcherDaoResponse.getNasdaqDataResponse());

	    insertNasdaqData(nasdaqOnlineDataFetcherDaoResponse, nasdaqRawDataJsonString);
	} catch (JsonProcessingException e)
	{
	    // TODO: Handle invalid data
	    // Invalid data
	} catch (Exception e)
	{
	    if (e.getMessage().contains("duplicate"))
	    {
		updateNasdaqData(nasdaqOnlineDataFetcherDaoResponse, nasdaqRawDataJsonString);
	    }
	}
    }

    private void updateNasdaqData(NasdaqOnlineDataFetcherDaoResponseEntity nasdaqOnlineDataFetcherDaoResponse, String nasdaqRawDataJsonString)
    {
	NasdaqDataUpdateDaoRequestEntity nasdaqDataUpdateDaoRequest = new NasdaqDataUpdateDaoRequestEntity();
	int offset = TimeZone.getDefault().getOffset(0L);
	Date targetDate = new Date(nasdaqOnlineDataFetcherDaoResponse.getNasdaqDataResponse().getTargetDate() - offset);
	nasdaqDataUpdateDaoRequest.setTargetDate(targetDate);
	nasdaqDataUpdateDaoRequest.setRawData(nasdaqRawDataJsonString);
	nasdaqDataUpdateDaoJpaImpl.send(nasdaqDataUpdateDaoRequest);
    }

    private void insertNasdaqData(NasdaqOnlineDataFetcherDaoResponseEntity nasdaqOnlineDataFetcherDaoResponse, String nasdaqRawDataJsonString)
    {
	NasdaqDataInsertDaoRequestEntity nasdaqDataInsertDaoRequest = new NasdaqDataInsertDaoRequestEntity();
	int offset = TimeZone.getDefault().getOffset(0L);
	Date targetDate = new Date(nasdaqOnlineDataFetcherDaoResponse.getNasdaqDataResponse().getTargetDate() - offset);
	nasdaqDataInsertDaoRequest.setTargetDate(targetDate);
	nasdaqDataInsertDaoRequest.setRawData(nasdaqRawDataJsonString);
	nasdaqDataInsertDaoJpaImpl.send(nasdaqDataInsertDaoRequest);
    }

    private String convertDataToJsonString(NasdaqDataResponse nasdaqDataResponse) throws JsonProcessingException
    {
	ObjectMapper mapper = new ObjectMapper();

	return mapper.writeValueAsString(nasdaqDataResponse);
    }

    private NasdaqOnlineDataFetcherDaoResponseEntity fetchNasdaqData()
    {
	NasdaqOnlineDataFetcherDaoRequestEntity nasdaqOnlineDataFetcherDaoRequest = new NasdaqOnlineDataFetcherDaoRequestEntity();
	nasdaqOnlineDataFetcherDaoRequest.setUrl(nasdaqDataUrl);
	nasdaqOnlineDataFetcherDaoRequest.setIndex(nasdaqEncodedFormDataIndex);
	nasdaqOnlineDataFetcherDaoRequest.setConnectTimeout(connectTimeout);
	nasdaqOnlineDataFetcherDaoRequest.setReadTimeout(readTimeout);

	NasdaqOnlineDataFetcherDaoResponseEntity nasdaqOnlineDataFetcherDaoResponse = nasdaqOnlineDataFetcherDaoRestImpl
		.send(nasdaqOnlineDataFetcherDaoRequest);

	return nasdaqOnlineDataFetcherDaoResponse;
    }
}
