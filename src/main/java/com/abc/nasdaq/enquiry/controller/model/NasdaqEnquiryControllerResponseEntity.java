package com.abc.nasdaq.enquiry.controller.model;

import java.util.List;

import com.abc.nasdaq.model.NasdaqIndexValue;
import com.fasterxml.jackson.annotation.JsonProperty;

public class NasdaqEnquiryControllerResponseEntity
{
    @JsonProperty("target_date")
    public String targetDate;

    @JsonProperty("message")
    public String message;

    @JsonProperty("nasdaq_index_value_list")
    public List<NasdaqIndexValue> nasdaqIndexValueList;

    public String getTargetDate()
    {
	return targetDate;
    }

    public void setTargetDate(String targetDate)
    {
	this.targetDate = targetDate;
    }

    public List<NasdaqIndexValue> getNasdaqIndexValueList()
    {
	return nasdaqIndexValueList;
    }

    public void setNasdaqIndexValueList(List<NasdaqIndexValue> nasdaqIndexValueList)
    {
	this.nasdaqIndexValueList = nasdaqIndexValueList;
    }

    public String getMessage()
    {
	return message;
    }

    public void setMessage(String message)
    {
	this.message = message;
    }

    @Override
    public String toString()
    {
	return "NasdaqEnquiryControllerResponseEntity [targetDate=" + targetDate + ", message=" + message + ", nasdaqIndexValueList="
		+ nasdaqIndexValueList + "]";
    }

}
