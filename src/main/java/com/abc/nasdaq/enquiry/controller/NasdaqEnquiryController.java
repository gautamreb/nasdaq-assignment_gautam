package com.abc.nasdaq.enquiry.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.abc.nasdaq.auth.service.AuthenticationService;
import com.abc.nasdaq.auth.service.model.AuthenticationServiceRequestEntity;
import com.abc.nasdaq.common.exception.AuthenticationFailException;
import com.abc.nasdaq.constant.NasdaqConstants;
import com.abc.nasdaq.enquiry.controller.model.NasdaqEnquiryControllerResponseEntity;
import com.abc.nasdaq.enquiry.service.NasdaqEnquiryService;
import com.abc.nasdaq.enquiry.service.model.NasdaqEnquiryServiceRequestEntity;
import com.abc.nasdaq.enquiry.service.model.NasdaqEnquiryServiceResponseEntity;
import com.wordnik.swagger.annotations.ApiImplicitParam;
import com.wordnik.swagger.annotations.ApiImplicitParams;
import com.wordnik.swagger.annotations.ApiOperation;

@Controller
public class NasdaqEnquiryController
{
    private Logger logger = Logger.getLogger(this.getClass());

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private NasdaqEnquiryService nasdaqEnquiryService;

    @ApiImplicitParams(value = {
	    @ApiImplicitParam(name = "x-auth-token", paramType = "header", dataType = "String", value = "Authentication Token", required = true),
	    @ApiImplicitParam(
		    name = "x-target-date",
		    paramType = "header",
		    dataType = "String",
		    value = "Request date to get NASDAQ data in yyyy-MM-dd format e.g. 2015-10-18",
		    required = true) })
    @ApiOperation(value = "NASDAQ Enquiry API", notes = "Retrieve NASDAQ index values in the specified date.")
    @RequestMapping(value = "/enquiry", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public NasdaqEnquiryControllerResponseEntity enquiry(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse)
    {
	String authToken = httpServletRequest.getHeader(NasdaqConstants.HEADER_X_AUTH_TOKEN);
	String targetDateString = httpServletRequest.getHeader(NasdaqConstants.HEADER_X_TARGET_DATE);
	logger.debug("authToken: " + authToken + " and targetDate: " + targetDateString);

	try
	{
	    auth(authToken);
	    Date targetDate = convertTargetDateStringToDate(targetDateString);
	    NasdaqEnquiryServiceResponseEntity nasdaqEnquiryServiceResponse = callNasdaqEnquiryService(targetDate);
	    return returnSuccessResponse(httpServletResponse, targetDateString, nasdaqEnquiryServiceResponse);
	} catch (AuthenticationFailException e)
	{
	    logger.error("Invalid auth token");
	    return returnFailInvalidAuthTokenResponse(httpServletResponse, targetDateString);
	} catch (ParseException e)
	{
	    logger.error("Error occurred while trying to parse date", e);
	    return returnFailIncorrectDateFormatResponse(httpServletResponse, targetDateString);
	}
    }

    private NasdaqEnquiryControllerResponseEntity returnFailInvalidAuthTokenResponse(HttpServletResponse httpServletResponse, String targetDateString)
    {
	NasdaqEnquiryControllerResponseEntity response = new NasdaqEnquiryControllerResponseEntity();
	response.setMessage(NasdaqConstants.RESPONSE_MESSAGE_INVALID_AUTH_TOKEN);
	response.setTargetDate(targetDateString);

	httpServletResponse.setStatus(HttpStatus.UNAUTHORIZED.value());

	return response;
    }

    private NasdaqEnquiryControllerResponseEntity returnFailIncorrectDateFormatResponse(HttpServletResponse httpServletResponse,
	    String targetDateString)
    {
	NasdaqEnquiryControllerResponseEntity response = new NasdaqEnquiryControllerResponseEntity();
	response.setMessage(NasdaqConstants.RESPONSE_MESSAGE_INCORRECT_DATE_FORMAT);
	response.setTargetDate(targetDateString);

	httpServletResponse.setStatus(HttpStatus.BAD_REQUEST.value());

	return response;
    }

    private NasdaqEnquiryControllerResponseEntity returnSuccessResponse(HttpServletResponse httpServletResponse, String targetDateString,
	    NasdaqEnquiryServiceResponseEntity nasdaqEnquiryServiceResponse)
    {

	NasdaqEnquiryControllerResponseEntity response = new NasdaqEnquiryControllerResponseEntity();
	response.setMessage(NasdaqConstants.RESPONSE_MESSAGE_OK);
	response.setTargetDate(targetDateString);
	response.setNasdaqIndexValueList(nasdaqEnquiryServiceResponse.getNasdaqIndexValueList());

	httpServletResponse.setStatus(HttpStatus.OK.value());

	return response;
    }

    private NasdaqEnquiryServiceResponseEntity callNasdaqEnquiryService(Date targetDate)
    {
	NasdaqEnquiryServiceRequestEntity nasdaqEnquiryServiceRequest = new NasdaqEnquiryServiceRequestEntity();
	nasdaqEnquiryServiceRequest.setTargetDate(targetDate);
	NasdaqEnquiryServiceResponseEntity nasdaqEnquiryServiceResponse = nasdaqEnquiryService.doService(nasdaqEnquiryServiceRequest);
	return nasdaqEnquiryServiceResponse;
    }

    private Date convertTargetDateStringToDate(String targetDate) throws ParseException
    {
	SimpleDateFormat sdf = new SimpleDateFormat(NasdaqConstants.INPUT_DATE_FORMAT);
	Date convertedTargetDate = sdf.parse(targetDate);

	return convertedTargetDate;
    }

    private void auth(String authToken) throws AuthenticationFailException
    {
	AuthenticationServiceRequestEntity authenticationServiceRequest = new AuthenticationServiceRequestEntity();
	authenticationServiceRequest.setAuthToken(authToken);
	authenticationService.doService(authenticationServiceRequest);
    }
}
