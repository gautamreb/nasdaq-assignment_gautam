package com.abc.nasdaq.enquiry.dao.model;

import java.util.Date;

public class NasdaqDataRetrievalDaoRequestEntity
{
    private Date targetDate;

    public Date getTargetDate()
    {
	return targetDate;
    }

    public void setTargetDate(Date targetDate)
    {
	this.targetDate = targetDate;
    }

}
