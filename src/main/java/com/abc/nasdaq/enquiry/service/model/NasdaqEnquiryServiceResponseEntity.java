package com.abc.nasdaq.enquiry.service.model;

import java.util.List;

import com.abc.nasdaq.model.NasdaqIndexValue;

public class NasdaqEnquiryServiceResponseEntity
{
    private List<NasdaqIndexValue> nasdaqIndexValueList;

    public List<NasdaqIndexValue> getNasdaqIndexValueList()
    {
	return nasdaqIndexValueList;
    }

    public void setNasdaqIndexValueList(List<NasdaqIndexValue> nasdaqIndexValueList)
    {
	this.nasdaqIndexValueList = nasdaqIndexValueList;
    }
}
