package com.abc.nasdaq.model;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class NasdaqRawData
{
    @JsonProperty("x")
    private long timestamp;

    @JsonProperty("y")
    @JsonSerialize(using = MoneySerializer.class)
    private BigDecimal value;

    public long getTimestamp()
    {
	return timestamp;
    }

    public void setTimestamp(long timestamp)
    {
	this.timestamp = timestamp;
    }

    public BigDecimal getValue()
    {
	return value;
    }

    public void setValue(BigDecimal value)
    {
	this.value = value;
    }

    @Override
    public String toString()
    {
	return "NasdaqRawData [timestamp=" + timestamp + ", value=" + value + "]";
    }

}
